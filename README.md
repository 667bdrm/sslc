# sslc

This is a Linux port of Fallout and Fallout 2 sslc script compiler

## Building

make

## Scripts preparation

In order to compile Fallout script source files on *nix, it must be converted to be able preprocessed on *nix filesystem (backslash separation, case sensitive includes).
You must have gcc installed!

To convert scripts do the following steps:

1. mkdir outscripts
2. mkdir inscripts
3. copy original scripts source tree to inscripts
4. run:

./convert-scripts.sh ./inscripts ./outscripts

## Compiling scripts

./p.sh <path/to/file.ssl>

## License

Linux port (c) 2006-2020, 667bdrm. The port files distributed under GNU Library General Public License v2 or later
