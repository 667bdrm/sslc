all: compile

clean:
	rm -f *.o sslc sslc_f1

compile:
	gcc -c compile.c extra.c lex.c parse.c parselib.c linux.c
	gcc -osslc compile.o extra.o lex.o parse.o parselib.o linux.o
