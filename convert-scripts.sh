#!/bin/bash

export SCRIPTSDIR=$1
export OUTDIR=$2


function convert_script {
  lcdir=`dirname $1 | tr '[:upper:]' '[:lower:]'`
  lcname=`basename $1 | tr '[:upper:]' '[:lower:]'`
  echo $lcdir
  mkdir -p "$OUTDIR/$lcdir"
  outscript="$OUTDIR/$lcdir/$lcname"
   echo "Generating $outscript"
  cp -pf $1 $outscript
  sed -i 's/\r$//g' $outscript
  # replace backslash to slash, then convert path to lowercase
  sed -i '/#include /s/\\/\//g' $outscript
  sed -i '/#include /s/\(.*\)/\L\1/g' $outscript
}

export -f convert_script


if [ $# -lt 2 ]
  then
    echo "Usage $0 <fallout scripts dir> <scripts destination dir>"
    exit 1
fi

find $SCRIPTSDIR -iname *.ssl -exec /bin/bash -c "convert_script \"{}\"" \;
find $SCRIPTSDIR -iname *.h -exec /bin/bash -c "convert_script \"{}\"" \;