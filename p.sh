#!/bin/sh


if [ $# -lt 1 ]
  then
    echo "Usage $0 <file.ssl>"
    exit 1
fi

noext=`echo $1 | cut -f1 -d.`
scriptdir=`dirname $1`
tempfile="$scriptdir/temp.c"
tempscript="temp.ssl"
noextfn=`basename $noext`

if [ ! -r $1 ]; then
  echo "$1 not found"
  exit 1
fi

cp $1 $tempfile

gcc -E $tempfile > $tempscript

./sslc $tempscript

if [ ! -r "temp.int" ]; then
  echo "Failed to compile $1"
  rm -f $tempscript
  rm -f $tempfile
  
  exit 1
else
  mkdir -p int
  cp temp.int "int/$noextfn.int"
fi
